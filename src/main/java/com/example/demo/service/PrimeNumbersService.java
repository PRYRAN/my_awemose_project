package com.example.demo.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Log4j2
@RequiredArgsConstructor
@Service
public class PrimeNumbersService {

    @Autowired
    public PrimeNumbersService(SimplePrimeNumberChecker checker) {
        this.checker = checker;
    }

    PrimeNumberChecker checker;


    /**
     * get number, check it and update result map
     * @param number
     * @param result
     */
    public void processNumberAndUpdateMap(Integer number, Map<Integer,Integer> result) {
        if (checker.isPrime(number)) {
            Utils.incrementMapValue(result,number);
        }
    }

}
