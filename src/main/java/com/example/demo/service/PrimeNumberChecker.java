package com.example.demo.service;

/**
 * Interface to check is number prime
 */
public interface PrimeNumberChecker {
    boolean isPrime(int n);
}
