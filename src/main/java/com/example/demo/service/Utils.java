package com.example.demo.service;

import java.util.Map;

public class Utils {

    /**
     * increment value of key by 1
     * @param map
     * @param key key
     * @param <K> type of key
     */
    public static <K> void incrementMapValue(Map<K, Integer> map, K key) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + 1);
        } else {
            map.put(key,1);
        }

    }
}
