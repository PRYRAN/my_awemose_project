package com.example.demo.controller;


import com.example.demo.service.PrimeNumbersService;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
@Log4j2
public class PrimeNumbersController {

    @Autowired
    PrimeNumbersService primeNumbersService;

    Gson gson = new Gson();


    /**
     * method to find occurrences of prime number
     * @param request
     * @return
     */
    @SneakyThrows
    @PostMapping("numbers")
    public ResponseEntity<Map<Integer,Integer>> findDuplicatePrimeNumbers(HttpServletRequest request) {
        log.info("request accepted");

        Map<Integer,Integer> result = null;
        try {
            result = readJsonStream(request.getInputStream());
        }catch ( IOException e) {
            log.error("error during serialization");
            e.printStackTrace();
        }


        HttpStatus status;
        if (result == null) {
            status = HttpStatus.BAD_REQUEST;
        }else {
            status = HttpStatus.OK;
        }
        log.info("status {}",status);
        log.info("returned result {}",result);

        return new ResponseEntity<>(
                result,
                status
        );

    }

    /**
     * fetch stream from servlet and parse
     * @param in input stream
     * @return result map (prime number -> number of occurences)
     * @throws IOException
     */
    public Map<Integer,Integer> readJsonStream(InputStream in) throws IOException {

        Map<Integer,Integer> result = new HashMap<>();


        log.info("start parsing");
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        reader.beginArray();

        while (reader.hasNext()) {

            Integer number = gson.<Integer>fromJson(reader, Integer.class);
            log.info("parsed number {}",number);
            primeNumbersService.processNumberAndUpdateMap(number,result);
            log.info("number were processed");
        }

        reader.endArray();
        reader.close();
        log.info("end parsing");

        return result;
    }
}
